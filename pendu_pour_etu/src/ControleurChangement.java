import java.util.NoSuchElementException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurChangement implements EventHandler<ActionEvent> {

    private Pendu appliPendu;
    private MotMystere modelependu;

    /**
     * @param p vue du jeu
     */
    public ControleurChangement(MotMystere modelependu, Pendu appliPendu) {
        this.modelependu = modelependu;
        this.appliPendu = appliPendu;
    }

    /**
     * L'action consiste à afficher une fenêtre popup précisant les règles du jeu.
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        this.appliPendu.setMaxErreur();
        this.modelependu.setMaxErreurm(this.appliPendu.getMaxErr());
        this.appliPendu.setMaxLettre();
        this.appliPendu.setMinLettre();
    }
}
