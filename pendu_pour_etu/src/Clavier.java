import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Circle ;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane{
    /**
     * il est conseillé de stocker les touches dans un ArrayList
     */
    private List<Button> clavier;

    /**
     * constructeur du clavier
     * @param touches une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     * @param tailleLigne nombre de touches par ligne
     */
    public Clavier(String touches, EventHandler<ActionEvent> actionTouches) {
        this.clavier = new ArrayList<>();
        for (char lettre : touches.toCharArray()) {
            Button button = new Button(""+lettre);
            button.setOnAction(actionTouches);
            button.setShape(new Circle(1.5));
            button.setMinSize(40, 30);
            button.setMaxSize(40, 30);
            this.clavier.add(button);
            this.getChildren().add(button);
        }
    }
    /**
     * permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees){
        for (Button bouton : this.clavier){
            if (touchesDesactivees.contains(bouton.getText())){bouton.setDisable(true);}
            else {bouton.setDisable(false);}
        }
    }
    public void desactiveTouche(){
        for (Button bouton : this.clavier){ 
            bouton.setDisable(true);
        }
    }
}
