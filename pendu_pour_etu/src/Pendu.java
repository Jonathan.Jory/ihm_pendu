import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import java.util.List;
import java.util.Arrays;
import java.io.File;
import java.util.ArrayList;


/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * banniere du titre
     **/
    private BorderPane banniere;
    /*
     * Slider de la police
     */
    private Slider slider;
    /*
     * Couleur de la baniere
     */
    private Color couleur;
    /**
     * le nombre min de lettres des mots
     */ 
    private int minLettre;
    /**
     * le nombre max de lettres des mots
     */ 
    private int maxLettre;
    /**
     * le nombre max de Erreur
     */ 
    private int maxErreur;
    /**
     * Label exemple de la taille de la police
     */ 
    private Label labPolice;
    /**
     * textfield max let
     */ 
    private TextField tfMaxLettre;
    /**
     * textfield min let
     */ 
    private TextField tfMinLettre;
    /**
     * textfield max erreur
     */ 
    private TextField tfMaxFailed;
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        this.minLettre = 3;
        this.maxLettre = 10;
        this.maxErreur = 10;   
        this.modelePendu = new MotMystere("./Mot.txt", this.minLettre, this.maxLettre, MotMystere.FACILE, this.maxErreur);
        this.lesImages = new ArrayList<Image>();
        this.panelCentral = new BorderPane();
        this.banniere = new BorderPane();
        this.chargerImages("./img");
        this.dessin = new ImageView(this.lesImages.get(0));
        this.niveaux = Arrays.asList("Facile", "Medium", "Difficile", "Expert");
        this.leNiveau = new Text(this.niveaux.get(this.modelePendu.getNiveau()));
        this.motCrypte = new Text(this.modelePendu.getMotCrypte());
        this.motCrypte.setFont(new Font("Arial", 22));
        this.clavier = new Clavier("ABCDEFGHIJKLMNOPQRSTUVWXYZ-", new ControleurLettres(this.modelePendu, this));


        Tooltip home = new Tooltip("Retour Accueil");
        this.boutonMaison = new Button("",new ImageView(new Image("home.png",25,25,false,false)));
        this.boutonMaison.setOnAction(new RetourAccueil(this.modelePendu, this));
        this.boutonMaison.setTooltip(home);

        Tooltip parametres = new Tooltip("Paramétres");
        this.boutonParametres = new Button("",new ImageView(new Image("parametres.png",25,25,false,false)));
        this.boutonParametres.setOnAction(new ControleurParametre(this));
        this.boutonParametres.setTooltip(parametres);

        this.bJouer = new Button("Lancer une partie");
        this.bJouer.setOnAction(new ControleurLancerPartie(this.modelePendu, this));
        this.chrono = new Chronometre();
        this.pg = new ProgressBar();
        this.couleur = Color.LIGHTBLUE;
        this.slider = new Slider();

        
        Tooltip maxLettre = new Tooltip("Entrer un entier pour définir un nombre max de lettre pour le mot mystère");
        Tooltip minLettre = new Tooltip("Entrer un entier pour définir un nombre min de lettre pour le mot mystère");
        Tooltip maxEr = new Tooltip("Entrer un entier pour définir un nombre max d'erreur");
        this.labPolice = new Label("Exemple");
        this.tfMaxLettre = new TextField("");
        this.tfMaxLettre.setTooltip(maxLettre);
        this.tfMinLettre = new TextField("");
        this.tfMinLettre.setTooltip(minLettre);
        this.tfMaxFailed = new TextField("");
        this.tfMaxFailed.setTooltip(maxEr);
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 800, 1000);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    private Pane titre(){
        HBox droite = new HBox();
        HBox gauche = new HBox();
        Text txt = new Text("Jeu du Pendu");
        txt.setFont(Font.font("Arial", FontWeight.NORMAL, 32));
        gauche.getChildren().add(txt);
        gauche.setAlignment(Pos.CENTER);

        Tooltip info = new Tooltip("Informations");
        Button boutonInfo = new Button("",new ImageView(new Image("info.png",25,25,false,false)));
        boutonInfo.setOnAction(new ControleurInfos(this));
        boutonInfo.setTooltip(info);
        droite.getChildren().addAll(this.boutonMaison, this.boutonParametres, boutonInfo);
        droite.setSpacing(5);
        droite.setAlignment(Pos.CENTER);

        this.banniere.setLeft(gauche);
        this.banniere.setRight(droite);
        this.banniere.setBackground(new Background(new BackgroundFill(this.couleur, null, null)));
        this.banniere.setPadding(new Insets(10));
        this.banniere.setMinHeight(80);
        return this.banniere;
    }

    /**
      * @return le panel du chronomètre
     */
    
    private TitledPane leChrono(){
 
        TitledPane res = new TitledPane();
        VBox vb = new VBox();
        vb.getChildren().add(this.chrono);
        res.setCollapsible(false);
        res.setContent(vb);
        res.setText("Chronometre");
        return res;
    }

    /**
    * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
    *         de progression et le clavier
    */
    private Pane fenetreJeu(){
        
        BorderPane jeu = new BorderPane();
        VBox center = new VBox();
        
        center.getChildren().addAll(this.motCrypte, this.dessin, this.pg, this.clavier);
        center.setPadding(new Insets(15));
        center.setSpacing(10);
        center.setAlignment(Pos.TOP_CENTER);

        VBox right = new VBox();
        Text niveau = new Text("Niveau "+ this.leNiveau.getText());
        niveau.setFont(new Font("Arial", 22));
        Button nvMot = new Button("Nouveau mot");
        nvMot.setOnAction(new ControleurLancerPartie(this.modelePendu, this));
        right.getChildren().addAll(niveau, leChrono(),nvMot);
        right.setAlignment(Pos.TOP_LEFT);
        right.setMinWidth(300);
        right.setPadding(new Insets(15));
        right.setSpacing(10);
        
        jeu.setCenter(center);
        jeu.setRight(right);
        return jeu;
    }
    private Pane fenetreParametre(){
        HBox hmodif = new HBox();

        Text min = new Text("Nombre minimum de lettres:");
        Text max = new Text("Nombre maximum de lettres:");
        Text err = new Text("Nombre maximum d'erreur:");

        VBox vtxt = new VBox();
        
        VBox vtf = new VBox();
        vtxt.getChildren().addAll(err, max, min);
        vtxt.setSpacing(15);
        vtf.getChildren().addAll(this.tfMaxFailed, this.tfMaxLettre, this.tfMinLettre);
        vtf.setSpacing(5);
        hmodif.getChildren().addAll(vtxt, vtf);
        hmodif.setSpacing(10);
        
  
        Button butChangement = new Button("Actualiser les changements");
        butChangement.setOnAction(new ControleurChangement(this.modelePendu, this));
      

        Text txtslid = new Text("Changer la taille de la police :            ");
        txtslid.setFont(new Font("Arial", 18));
        slider.setMin(10);
        slider.setMax(110);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(false);
        slider.setBlockIncrement(10);
        slider.valueProperty().addListener(new ControlSlider(this)); 
        slider.setMajorTickUnit(5);
    

        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(this.couleur);
        colorPicker.setOnAction(new ControleurColorPicker(colorPicker, this));
        
        Text colorTxt = new Text("Changer la couleur :                         ");
        colorTxt.setFont(new Font("Arial", 18));

        HBox hb = new HBox();
        hb.getChildren().addAll(colorTxt, colorPicker);
        hb.setSpacing(10);
        HBox hbslid = new HBox();
        hbslid.getChildren().addAll(txtslid, slider);

        FlowPane root = new FlowPane();
        root.setPadding(new Insets(10));

        VBox vb = new VBox();
        vb.setPadding(new Insets(10));
        vb.setSpacing(10);
        vb.getChildren().addAll(hmodif, butChangement, hb, hbslid, this.labPolice);
        vb.setSpacing(10);
        root.getChildren().add(vb);
        return root;
    }

    /**
      * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
      */
    private Pane fenetreAccueil(){
        
        VBox res = new VBox();
        TitledPane tp = new TitledPane();
        VBox vb = new VBox();
        ToggleGroup tg = new ToggleGroup();

        RadioButton facile = new RadioButton("Facile");
        facile.setOnAction(new ControleurNiveau(this.modelePendu, this));
        facile.setSelected(true);
        RadioButton medium = new RadioButton("Medium");
        medium.setOnAction(new ControleurNiveau(this.modelePendu, this));
        RadioButton difficile = new RadioButton("Difficile");
        difficile.setOnAction(new ControleurNiveau(this.modelePendu, this));
        RadioButton expert = new RadioButton("Expert");
        expert.setOnAction(new ControleurNiveau(this.modelePendu, this));

        facile.setToggleGroup(tg);
        medium.setToggleGroup(tg);
        difficile.setToggleGroup(tg);
        expert.setToggleGroup(tg);

        vb.getChildren().addAll(facile, medium, difficile, expert);
        tp.setCollapsible(false);
        tp.setContent(vb);
        tp.setText("Niveau de difficulté");
        res.getChildren().addAll(this.bJouer, tp);
        res.setPadding(new Insets(20));
        res.setSpacing(15);
        return res;
    }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil(){
        this.panelCentral.setCenter(fenetreAccueil());
        this.boutonParametres.setDisable(false);
        this.boutonMaison.setDisable(true);
    }
    
    public void modeJeu(){
        this.panelCentral.setCenter(fenetreJeu());
        this.boutonMaison.setDisable(false);
        this.boutonParametres.setDisable(true);
    }
    
    public void modeParametres(){
        this.panelCentral.setCenter(fenetreParametre());
        this.boutonParametres.setDisable(true);
        this.boutonMaison.setDisable(false);
    }

    /** lance une partie */
    public void lancePartie(){
        this.modelePendu.setMotATrouver();
        this.chrono.resetTime();
        this.chrono.start();
        this.majAffichage();
        this.modeJeu();
    }

    /*
     * @param couleur
     *  change la couleur de la baniere
     */
    public void setCouleur(Color couleur){
        this.banniere.setBackground(new Background(new BackgroundFill(couleur, null, null)));
        this.couleur = couleur;
    }
    public int getMaxErr(){return this.maxErreur;}
    /**
     * @return le nombre max de lettres
     */
    public int getMaxlet(){
        return this.maxLettre;
    }
    /**
     * @return le nombre min de lettres
     */
    public int getMinlet(){
        return this.minLettre;
    }
    /**
     * change le nombre max d'erreur
     */
    public void setMaxErreur(){
        if (!(this.tfMaxFailed.getText().equals(""))){
            try{this.maxErreur=Integer.parseInt(this.tfMaxFailed.getText());}
            catch(Exception e){this.popUpNbMaxErreur().showAndWait();}
        }
    }
    /*
     * change le nombre max de lettres
     */
    public void setMaxLettre(){
        if (!(this.tfMaxLettre.getText().equals(""))){
            try{this.maxLettre=Integer.parseInt(this.tfMaxLettre.getText());}
            catch(Exception e){this.popUpNbMaxErreur().showAndWait();}
        }
    }
    /**
     * change le nombre min de lettres
     */
    public void setMinLettre(){
        if (!(this.tfMinLettre.getText().equals(""))){
            try{this.minLettre=Integer.parseInt(this.tfMinLettre.getText());}
            catch(Exception e){this.popUpNbMaxErreur().showAndWait();}
        }
    }
    

    /**
     * change la police
     */
    public void changePolice(int newValue){
        this.motCrypte.setFont(Font.font(newValue));
        this.leNiveau.setFont(Font.font(newValue));
        this.labPolice.setFont(Font.font(newValue));
    }
    /**
     * change le nombre MAX de lettres
     */
    public void changeMaxLet(){
        this.maxLettre=Integer.parseInt(this.tfMaxLettre.getText());
    }
    /**
     * change le nombre min de lettres
     */
    public void changeMinLet(){
        this.minLettre=Integer.parseInt(this.tfMinLettre.getText());
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        this.leNiveau = new Text(this.niveaux.get(this.modelePendu.getNiveau()));
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
        this.dessin.setImage(this.lesImages.get(this.lesImages.size()-this.modelePendu.getNbErreursRestants()-1));
        this.pg.setProgress(1-(float) this.modelePendu.getNbErreursRestants()/this.modelePendu.getNbErreursMax());
    }

    /**
     * accesseur du chronomètre (pour les controleur du jeu)
     * @return le chronomètre du jeu
     */
    public Chronometre getChrono(){
        return this.chrono;
    }

    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Vous devez trouver le bon mot \n avant que le personnage ne soit pendu. \n \n Astuce: La barre de progression \n vous indique le nombre d'essaie restant.");
        alert.setTitle("Règles");
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Bravo!!! Vous avez gagnez.");        
        alert.setHeaderText("GAGNÉ !!!");
        alert.setTitle("Victoire");
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Oh non!!! \n Vous avez perdu, quel dommage. \n Le mot était:  " + this.modelePendu.getMotATrouve());        
        alert.setHeaderText("PERDU !!!");
        alert.setTitle("Défaite");
        return alert;
    }
    public Alert popUpNbMaxErreur(){
        Alert alert = new Alert(Alert.AlertType.WARNING, "Entrez des nombres entre 1 et 10 compris pour les paramétres.");
        alert.setTitle("Erreur");
        return alert;
    }
    /*
     * descative toutes les lettres du clavier 
     */
    public void desactiverLettres(){
        this.clavier.desactiveTouche(); 
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        this.modeAccueil();
        stage.setScene(this.laScene());
        stage.show();
    }
    public void quitter(){
        Platform.exit();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}