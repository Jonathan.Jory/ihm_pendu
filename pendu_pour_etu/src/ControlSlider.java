import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class ControlSlider implements ChangeListener<Number>{
    
    private Pendu vuePendu;

    public ControlSlider(Pendu vuePendu){
        this.vuePendu = vuePendu;
    }
        
    @Override
    public void changed(ObservableValue arg0, Number oldValue, Number newValue) {
        this.vuePendu.changePolice(newValue.intValue());
    }
}
