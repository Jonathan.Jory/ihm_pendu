import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ColorPicker;


public class ControleurColorPicker implements EventHandler<ActionEvent>{
    
    private ColorPicker colorPicker;
    private Pendu vuePendu;

    public ControleurColorPicker(ColorPicker colorPicker, Pendu vuePendu){
        this.colorPicker = colorPicker;   
        this.vuePendu = vuePendu;
    }

    @Override
    public void handle(ActionEvent event) {
        this.vuePendu.setCouleur(colorPicker.getValue());
        this.vuePendu.majAffichage();
    }

}
